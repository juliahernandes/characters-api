const { response } = require('express');

const express = require('express'),
    axios = require('axios'),
    app = express();
 
app.get('/', async function (req, res) {
  const characters = await getCharacters();
  console.log(characters);
  res.send(characters);
});

const api = axios.create({
    baseURL: 'https://rickandmortyapi.com/api'
});


async function getCharacters () {
    const response = await api.get('/character');
    const characters = response.data.results;

    const formattedData = characters.map(character => {
        const { name, status, species, type, gender, origin, location } = character;
        return { name, status, species, type, gender, origin, location };
    });

    return formattedData;
}

app.listen(3000);